# from django.shortcuts import render
from todos.models import TodoItem, TodoList
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy


class TodoListView(ListView):
    model = TodoList
    template_name = "todo/list.html"


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todo/detail.html"


class TodoCreateView(CreateView):
    model = TodoList
    template_name = "todo/create.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])

    # success_url = reverse_lazy("todo_list_list")


class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = "todo/update.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])

    # success_url = reverse_lazy("todo_list_list")


class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = "todo/delete.html"
    fields = ["name"]
    success_url = reverse_lazy("todo_list_list")


class ItemCreateView(CreateView):
    model = TodoItem
    template_name = "todo_item/create.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todo_item/update.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])
