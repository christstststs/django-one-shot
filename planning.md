## Feature 1 Setting up
* [x] Fork and clone the starter project from django-one-shot 
* [x] Create a new virtual environment in the repository directory for the project
   * [x] python -m venv ./.venv
* [x] Activate the virtual environment
   * [x] .\.venv\Scripts\Activate.ps1
* [x] Upgrade pip
    * [x] python -m pip install --upgrade pip
* [x] Install django
    * [x] python -m pip install
* [x] Install black
    * [x] pip.install black
* [x] Install flake8
    * [x] pip.install flake8
* [x] Install djlint
    * [x] pip install djlint
* [x] Deactivate your virtual environment
    * [x] deactivate
* [x] Activate your virtual environment
    * [x] .\.venv\Scripts\Activate.ps1
* [x] Use pip freeze to generate a requirements.txt file
    * [x] pip freeze > requirements.txt
  
### Test feature 1
* [x] python manage.py test tests.test_feature_01

## Feature 2
* [x] Create a Django project named brain_two so that the manage.py file is in the top directory
  * [x] django-admin startproject brain_two .
* [x] Create a Django app named todos and install it in the brain_two Django project in the 
  * [x] python manage.py startapp todos
* [x] INSTALLED_APPS list
  * [x] "todos.apps.TodosConfig", -- add this under INSTALLED_APPS 
* [x] Run the migrations
  * [x] python manage.py migrate
* [x] Create a super user
  * [x] python manage.py createsuperuser

### Test feature 2
* [x] python manage.py test tests.test_feature_02

## Feature 3
* [x] Create a model named TodoList
  * [x] add name field
  * [x] add created_on field
    * [x] set it automatically time
  * [x] add a function 

### Feature 3 Test
* [x] python manage.py shell
* [X] >>> from todos.models import TodoList
* [X] >>> todolist = TodoList.objects.create(name="Reminders")


## Feature 4
* [x] Register the TodoList model with the admin
  * [x] Import TodoList model
  * [x] register the model by adding this - admin.site.register(TodoList)

### Feature 4 Testing
* [X] Create a to do list on admin

## Feature 5
* [X] Create a TodoItem model for task, due_date,is_completed, list(ForeignKey)
  * [X] task = models.CharField(max_length=100)
  * [X] due_date = models.DateTimeField()
  * [X] is_completed = models.BooleanField(default=False)
  *  [X] list = models.ForeignKey(TodoList, related_name=("items"), on_delete=models.CASCADE)
  *  [X] function - ef __str__(self):return self.task

### Feature 5 Testing
* [X] from todos.models import TodoList,TodoItem
* [x] todolist = TodoList.objects.first()
* [x] todoitem = TodoItem.objects.create(task="Take out the Garbage", list=todolist)

## Feature 6
* [x] Register the TodoItem model with the admin
  * [x] Import TodoItem model
  * [x] register the model by adding this - admin.site.register(TodoItem)

### Feature 6 Testing
* [X] Create a to do item on admin
* [x] Select a to do list that the item should belong to

## Feature 7
* [x] Create a view that will get all of the instances of the TodoList model and put them in the context for the template.
  * [x] create this class in view.
     *  class TodoListView(ListView):
            model = TodoList
            template_name = "todo/list.html"
* [x] Register that view in the todos app for the path "" and the name "todo_list_list" in a new file named todos/urls.py. 
  * [x]   path("", TodoListView.as_view(), name="todo_list_list"), add this in url.py
* [x] Include the URL patterns from the todos app in the brain_two project with the prefix "todos/".
Create a template for the list view that complies with the following specifications.
    * In project url.py, add the following  path("todos/", include("todos.urls")),

## Feature 8
* [x] Create a view that shows the details of a particular to-do list, including its tasks
* [x] In the todos urls.py file, register the view with the path "<int:pk>/" and the name "todo_list_detail"
* [x] Create a template to show the details of the todolist and a table of its to-do items
* [x] Update the list template to show the number of to-do items for a to-do list
* [x] Update the list template to have a link from the to-do list name to the detail view for that to-do list


## Feature 9
* [x] Create a create view for the TodoList model that will show the name field in the form and handle the form submission to create a new TodoList
* [x] If the to-do list is successfully created, it should redirect to the detail page for that to-do list
* [x] Register that view for the path "create/" in the todos urls.py and the name "todo_list_create"
* [x] Create an HTML template that shows the form to create a new TodoList (see the template specifications below)
* [x] Add a link to the list view for the TodoList that navigates to the new create view

## Feature 10
* [x] Create an update view for the TodoList model that will show the name. field in the form and handle the form submission to change an existing TodoList
* [x] If the to-do list is successfully edited, it should redirect to the detail page for that to-do list.
* [x] Register that view for the path /todos/<int:pk>/edit in the todos urls.py and the name "todo_list_update".
* [x] Create an HTML template that shows the form to edit a new TodoList (see the template specifications below).
* [x] Add a link to the list view for the TodoList that navigates to the new update view.

## Feature 11
* [x] Create a delete view for the TodoList model that will show a delete button and handle the form submission to delete an existing TodoList.
* [x] If the to-do list is successfully deleted, it should redirect to the to-do list list view.
* [x] Register that view for the path /todos/<int:pk>/delete in the todos urls.py and the name "todo_list_delete".
* [x] Create an HTML template that shows the form to delete a new TodoList (see the template specifications below).
* [x] Add a link to the detail view for the TodoList that navigates to the new delete view.

## Feature 12
* [x]  Create a create view for the TodoItem model that will show the task field in the form and handle the form submission to create a new TodoItem.
* [x] If the to-do list is successfully created, it should redirect to the detail page for that to-do list.
* [x] Register that view for the path "items/create/" in the todos urls.py and the name "todo_item_create".
* [x] Create an HTML template that shows the form to create a new TodoItem (see the template specifications below).
* [x] Add a link to the list view for the TodoList that navigates to the new create view.

## Feature 13
* [x] Create an update view for the TodoItem model that will show the task field, the due date field, an is_completed checkbox, and a select list showing the to-do lists in the form and handle the form submission to change an existing TodoItem.
* [x] If the to-do item is successfully edited, it should redirect to the detail page for the to-do list.
* [x] Register that view for the path /todos/items/<int:pk>/edit in the todos urls.py and the name "todo_item_update".
* [x] Create an HTML template that shows the form to edit a TodoItem (see the template specifications below).
* [x] Add a link to the list view for the TodoList that navigates to the new update view.